//
//  MapViewController.swift
//  Restaurant
//
//  Created by Mohsan on 20/11/2016.
//  Copyright © 2016 Mohsan. All rights reserved.
//

import UIKit
import MapKit


class MapViewController: UIViewController, TabBarViewController, MKMapViewDelegate {
    
    @IBOutlet weak var mapView: MKMapView!

    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var labelRestaurantTitle: UILabel!
    
    @IBOutlet weak var labelRestaurantAddress: UILabel!
    
    @IBOutlet weak var labelRestaurantDesc: UILabel!
    
    var rootController : RootViewController!
        
    @IBOutlet weak var constraintBottomScrollView: NSLayoutConstraint!
    
    var mapAnnotationRestaurant: [MKPointAnnotation:String] = [:]
    

    var descVisible: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Position des restaurants"
        self.rootController = self.tabBarController as! RootViewController
        self.mapView.showsUserLocation = true
        self.mapView.delegate = self
        
        onRestaurantReloaded()
        let span = MKCoordinateSpanMake(0.160, 0.160)
        let region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: 48.859971662583554, longitude: 2.3418045043945312), span: span)
        mapView.setRegion(region, animated: true)
        
        self.edgesForExtendedLayout = [UIRectEdge.top]
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func onRestaurantReloaded() {
        if (rootController != nil) {
            mapView.removeAnnotations(mapAnnotationRestaurant.keys.reversed())
            mapAnnotationRestaurant.removeAll()
            for rest in rootController.restaurants {
                let pointAnnoation = MKPointAnnotation()
                pointAnnoation.title = rest.name
                
                
                pointAnnoation.coordinate = CLLocationCoordinate2D(latitude: rest.lat, longitude: rest.lon)
                 
                
                mapView.addAnnotation(pointAnnoation)
                if let id_host = rest.id_host {
                    mapAnnotationRestaurant[pointAnnoation] = id_host
                }
                
            }
        }
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        for (key, value) in mapAnnotationRestaurant {
            let coordinate: CLLocationCoordinate2D = (view.annotation?.coordinate)!
            if (key.coordinate.latitude == coordinate.latitude && key.coordinate.longitude == coordinate.longitude) {
                for (restaurant) in rootController.restaurants {
                    
                    if let id = restaurant.id_host{
                        if (id == value) {
                            showDesc()
                            
                            labelRestaurantTitle.text = restaurant.name! + " de " + restaurant.author!
                            labelRestaurantAddress.text = restaurant.address
                            labelRestaurantDesc.text = restaurant.short_description
                        }

                    }else{
                        labelRestaurantTitle.text = restaurant.name! + " de " + restaurant.author!
                        labelRestaurantAddress.text = restaurant.address
                        labelRestaurantDesc.text = restaurant.short_description
                    }
                }
            }
        }
    }
    
    func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView) {
        hideDesc()
    }
    
    
    func showDesc() {
        if (!descVisible) {
            constraintBottomScrollView.constant = 0
            view.layoutIfNeeded()
            descVisible = true
        }
    }
    
    func hideDesc() {
        if (descVisible) {
            constraintBottomScrollView.constant = -150
            view.layoutIfNeeded()
            descVisible = false
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        print("MAP")
        onRestaurantReloaded()
    }

}
