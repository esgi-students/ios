//
//  AddViewController.swift
//  Restaurant
//
//  Created by Mohsan on 27/11/2016.
//  Copyright © 2016 Mohsan. All rights reserved.
//

import Alamofire
import SwiftyJSON
import CoreLocation
import MapKit

class AddViewController: UIViewController, UITextFieldDelegate, UITextViewDelegate, UITableViewDelegate, UITableViewDataSource {
    
    let identifer : String = "streetID"
    var edit : Bool = false
    var restEdit : Restaurant?
    
    @IBOutlet weak var editButton: UIButton!

    @IBOutlet weak var fieldName: UITextField!
    
    @IBOutlet weak var fieldDescription: UITextView!
    
    @IBOutlet weak var fieldAddress: UITextField!
    
    @IBOutlet weak var tableViewStreetResult: UITableView!;
    var searchCompleter = MKLocalSearchCompleter();
    var searchResults = [MKLocalSearchCompletion]();
    
    var preventUpdateSearch : Bool = false;
    
    var addressPlacemark : MKPlacemark?;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Add Restaurant"
        
        self.tableViewStreetResult.delegate = self
        self.tableViewStreetResult.dataSource = self
        
        self.hideKeyboardWhenTappedAround()
        
        self.fieldAddress.delegate = self
        self.searchCompleter.delegate = self
        

        if self.edit {
            self.fieldName.text = self.restEdit!.name
            self.fieldDescription.text = self.restEdit!.short_description
            self.fieldAddress.text = self.restEdit!.address
            
            self.title = "Update restaurant"
            self.editButton.setTitle("Update restaurant", for: .normal)
            
        }
        
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func addRestaurant(_ sender: UIButton) {
        
        print(requireFields())
        if(requireFields()) {
            
            
            if !self.edit {
                print("CREATE")
                SyncEngine.createRestaurant(name: self.fieldName.text!,
                                            desc: self.fieldDescription.text!,
                                            country: (self.addressPlacemark != nil) ? self.addressPlacemark!.countryCode! : "Paris",
                                            address: self.fieldAddress.text!,
                                            lat: (self.addressPlacemark != nil) ? self.addressPlacemark!.coordinate.latitude : 0,
                                            lon: (self.addressPlacemark != nil) ? self.addressPlacemark!.coordinate.longitude : 0,
                                            end: { (restaurant) in
                                                self.alertMessage(title: "Add", message: "The restaurant was add", handler: { (UIAlertAction) in
                                                    _ = self.navigationController?.popViewController(animated: true)
                                                })
                            
                })
            } else {
                print("UPDATE")
                SyncEngine.updateRestaurant(restaurant: self.restEdit!,
                                            name: self.fieldName.text!,
                                            desc: self.fieldDescription.text!,
                                            address: self.fieldAddress.text!,
                                            country: self.addressPlacemark != nil ? self.addressPlacemark!.countryCode! : self.restEdit!.country!,
                                            lat: addressPlacemark != nil ? addressPlacemark!.coordinate.latitude : self.restEdit!.lat,
                                            lon: addressPlacemark != nil ? addressPlacemark!.coordinate.longitude : self.restEdit!.lon,
                                            end: { (restaurant) in
                                                self.alertMessage(title: "Update", message: "The restaurant was update", handler: { (UIAlertAction) in
                                                    _ = self.navigationController?.popViewController(animated: true)
                                                })
                                                
                })
            }
        }
    }
    
    func requireFields() -> Bool {
        
        guard let textName = fieldName.text, !textName.isEmpty else {
            return false
        }
        
        if (self.fieldDescription.text == "description"){
            
            return false
        }

        
        if (!self.edit && self.addressPlacemark == nil && Reachability.isConnectedToNetwork()) {
            
            
            return false
        }
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (!self.preventUpdateSearch) {
            self.searchCompleter.queryFragment = textField.text!;
        }
        return true;
    }

    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.searchResults.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell = self.tableViewStreetResult.dequeueReusableCell(withIdentifier: self.identifer) ?? UITableViewCell(style: .subtitle, reuseIdentifier: self.identifer)
        
        let searchResult = searchResults[indexPath.row];
        cell.textLabel?.text = searchResult.title;
        cell.detailTextLabel?.text = searchResult.subtitle;
        return cell;
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let searchResult = searchResults[indexPath.row];
        let searchRequest = MKLocalSearchRequest(completion: searchResult);
        let search = MKLocalSearch(request: searchRequest);
        search.start { (response, error) in
            self.preventUpdateSearch = true;
            self.fieldAddress.text = searchResult.title + " " + searchResult.subtitle;
            self.preventUpdateSearch = false;
            self.searchResults.removeAll();
            self.tableViewStreetResult.reloadData();
            self.addressPlacemark = response?.mapItems[0].placemark;
        }
    }
    
    func editRestaurant(rest: Restaurant){
        self.edit = true
        self.restEdit = rest
    }
    

}
