//
//  SearchViewController+MKLocalSearchCompleterDelegate.swift
//  Restaurant
//
//  Created by Quentin on 28/11/2016.
//  Copyright © 2016 Mohsan. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit

extension AddViewController: MKLocalSearchCompleterDelegate {
    
    func completerDidUpdateResults(_ completer: MKLocalSearchCompleter) {
        searchResults = completer.results;
        tableViewStreetResult.reloadData();
        addressPlacemark = nil;
    }
    
    func completer(_ completer: MKLocalSearchCompleter, didFailWithError error: Error) {
        // handle error
    }
}
