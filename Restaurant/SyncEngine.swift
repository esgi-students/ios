//
//  SyncEngine.swift
//  Restaurant
//
//  Created by Mohsan on 04/12/2016.
//  Copyright © 2016 Mohsan. All rights reserved.
//

import Foundation
import CoreData
import Alamofire
import SwiftyJSON
import MapKit

class SyncEngine: NSObject {
    
    static func fullSync(res: @escaping ([Restaurant]) -> Void) {
        let hasNetwork: Bool = Reachability.isConnectedToNetwork();
        
        //Load Data From CoreData
        print("fullSync")
        print("Load Restaurants from CoreData")
        SyncEngine.loadFromCoreData(predicate: nil, res: { (restaurants) in
            let myGroup: DispatchGroup = DispatchGroup()
            
            if (!hasNetwork) {
                print("Network Not Available -> Cancel Possible Update -> Return Only CoreData Objects")
                res(restaurants)
                return;
            }
            
            for restaurant in restaurants {
                print(restaurant)
                
                if (restaurant.status != 0) { //Check if restaurant has been update off connection
                    if (restaurant.status == 1) { //Updated
                        print("Need Update")
                        print("Lock Group")
                        myGroup.enter()
                        SyncEngine.updateRestaurant(restaurant: restaurant, name: restaurant.name!, desc: restaurant.short_description!, address: restaurant.address!, country: restaurant.country!, lat: restaurant.lat, lon: restaurant.lon, end: { (restaurant) in
                            print("End Updated -> Leave Group")
                            myGroup.leave()
                        })
                    } else if (restaurant.status == 2) { //Deleted
                        print("Need Delete")
                        print("Lock Group")
                        myGroup.enter()
                        SyncEngine.removeRestaurant(restaurant: restaurant, end: {
                            print("End Deleted -> Leave Group")
                            myGroup.leave()
                        })
                    } else if (restaurant.status == 3) { //New
                        print("Need Create")
                        print("Lock Group")
                        myGroup.enter()
                        SyncEngine.createRestaurant(name: restaurant.name!, desc: restaurant.short_description!, country: restaurant.country!, address: restaurant.address!, lat: restaurant.lat, lon: restaurant.lon, end: { (restaurant) in
                            
                            print("End Create -> Leave Group")
                            myGroup.leave()
                        })
                    }
                }
            }
            
            myGroup.notify(queue: DispatchQueue.main, execute: { 
                print("Finish all Push Modification")
                
                print("Pull new Restaurants from API")
                SyncEngine.pullRestaurants(res: { (restaurants_) in
                    res(restaurants_)
                })
            })
        })
    }
    
    
    static func pullRestaurants(res: @escaping ([Restaurant]) -> Void) {
        print("pullRestaurants")
        Alamofire.request(Helper.base_url, method: .get)
            .validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"])
            .responseJSON { response in
                switch response.result {
                case .success(let value):
                    var restaurantsReturn: [Restaurant] = []
                    if let context = DataManager.shared.objectContext {
                        let request: NSFetchRequest<Restaurant> = Restaurant.fetchRequest()
                        print("Clean All Restaurants")
                        if let restaurants = try? context.fetch(request) {
                            for restaurant in restaurants {
                                print("Remove Restaurant ")
                                print(restaurant)
                                context.delete(restaurant)
                            }
                        }
                        print("--")
                        print("Insert new Restaurant from API")
                        for (_,json) in JSON(value) {
                            print("Insert Restaurant id = " + json["_id"].rawString()!)
                            restaurantsReturn.append(SyncEngine.insertRestaurant(json: json)!)
                        }
                    }
                    res(restaurantsReturn)
                    
                case .failure(let error):
                    print(error)
                }
        }
    }
    
    
    static func loadFromCoreData(predicate: NSPredicate?, res: ([Restaurant]) -> Void) {
        if let context = DataManager.shared.objectContext {
            let request: NSFetchRequest<Restaurant> = Restaurant.fetchRequest()
            if (predicate != nil) {
                request.predicate = predicate
            }
            if let restaurants = try? context.fetch(request) {
                res(restaurants)
            }
        }
    }
    
    static func loadVisible(res: ([Restaurant]) -> Void) {
        SyncEngine.loadFromCoreData(predicate: NSPredicate(format: "status != %i", 2), res: res)
    }
    
    static func insertRestaurant(json: JSON) -> Restaurant? {
        if let context = DataManager.shared.objectContext {
            let restaurant = Restaurant(context: context)
            restaurant.name = json["name"].rawString()
            restaurant.author = "author"
            restaurant.short_description = json["description"].rawString()
            restaurant.address = json["address"].rawString()
            restaurant.country = json["country"].rawString()
            restaurant.lat = json["loc"][0].double!
            restaurant.lon = json["loc"][1].double!
            restaurant.id_host = json["_id"].rawString()
            restaurant.status = 0
            _ = try! context.save()
            return restaurant;
        }
        
        return nil
    }
    
    
    static func removeRestaurant(restaurant: Restaurant, end: @escaping () -> ()) {
        
        if (Reachability.isConnectedToNetwork()) {
            if let id = restaurant.id_host {
                let parameters: Parameters = [
                    "id_host" : id
                ]
                
                
                Alamofire.request(Helper.base_url, method: .delete, parameters: parameters, encoding: URLEncoding.httpBody)
                    .validate(statusCode: 200..<300)
                    .responseJSON { response in
                        switch response.result {
                        case .success( _):
                            DataManager.shared.objectContext!.delete(restaurant)
                            end()
                            
                        case .failure(let error):
                            print(error)
                            end()
                        }
                        
                        
                }

            }
            _ = try! DataManager.shared.objectContext?.save()
        } else {
            if let context = DataManager.shared.objectContext {
                restaurant.status = 2;
                _ = try! context.save()
                end()
            }
        }
    }
    
    static func createRestaurant(name: String, desc: String, country: String, address: String, lat: Double, lon: Double, end: @escaping (Restaurant?) -> ()) {
        
        if (Reachability.isConnectedToNetwork()) {
            let parameters: Parameters = [
                "name": name,
                "description": desc,
                "country": country,
                "address" : address,
                "loc" : [
                    lat,
                    lon
                ]
            ]
            Alamofire.request(Helper.base_url, method: .post, parameters: parameters, encoding: URLEncoding.httpBody)
                .validate(statusCode: 200..<300)
                .responseJSON { response in
                    switch response.result {
                    case .success(let value):
                        print(value)
                        end(SyncEngine.insertRestaurant(json: JSON(value)))
                        
                    case .failure(let error):
                        print(error)
                        end(nil)
                    }
            }
        } else {
            if let context = DataManager.shared.objectContext {
                let restaurant = Restaurant(context: context)
                restaurant.name = name
                restaurant.author = "author"
                restaurant.short_description = desc
                restaurant.address = address
                restaurant.country = country
                restaurant.lat = lat
                restaurant.lon = lon
                restaurant.status = 3
            
                _ = try! context.save()
                end(restaurant)
            }
        }
    }
    
    
    static func updateRestaurant(restaurant: Restaurant, name: String, desc: String, address: String, country: String, lat: Double, lon: Double, end: @escaping (Restaurant?) -> ()) {
        
        if (Reachability.isConnectedToNetwork() && restaurant.id_host != nil) {
            let parameters: Parameters = [
                "name": name,
                "description": desc,
                "address" : address,
                "country" : country,
                "loc" : [
                    lat,
                    lon
                ],
                "id_host" : restaurant.id_host!
            ]
            
            Alamofire.request(Helper.base_url, method: .put, parameters: parameters, encoding: URLEncoding.httpBody)
                .validate(statusCode: 200..<300)
                .responseJSON { response in
                    switch response.result {
                    case .success(_):
                        restaurant.name = name
                        restaurant.short_description = desc
                        restaurant.address = address
                        restaurant.country = country
                        restaurant.lat = lat
                        restaurant.lon = lon
                        _ = try! DataManager.shared.objectContext?.save()
                        end(restaurant)
                        
                        
                    case .failure(let error):
                        print(error)
                        end(nil)
                    }
            }
        } else {
            if let context = DataManager.shared.objectContext {
                restaurant.name = name
                restaurant.author = "author"
                restaurant.short_description = desc
                restaurant.address = address
                restaurant.country = country
                restaurant.lat = lat
                restaurant.lon = lon
                
                if (restaurant.id_host != nil) {
                    restaurant.status = 1
                }
                _ = try! context.save()
                end(restaurant)
            }
        }
    }
}
