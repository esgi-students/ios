//
//  MainViewController.swift
//  Restaurant
//
//  Created by Mohsan on 20/11/2016.
//  Copyright © 2016 Mohsan. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import CoreData

class ListRestaurantViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, TabBarViewController {
    
    @IBOutlet weak var tableViewRestaurant: UITableView!
    
    
    let identifer : String = "restaurantID"
    var rootController : RootViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Liste des restaurants"
        
        self.rootController = self.tabBarController as! RootViewController
        
        self.tableViewRestaurant.delegate = self
        self.tableViewRestaurant.dataSource = self
        
        let button = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(ListRestaurantViewController.addItem)  )
        
        let button_refresh = UIBarButtonItem(barButtonSystemItem: .refresh, target: self.rootController, action: #selector(self.rootController.refresh)  )
        
        self.navigationItem.rightBarButtonItem = button
        self.navigationItem.leftBarButtonItem  = button_refresh
        
        onRestaurantReloaded()
    }
    
    
    
    func addItem(){
        self.navigationController?.pushViewController(AddViewController(), animated: true)
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.rootController.restaurants.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell = self.tableViewRestaurant.dequeueReusableCell(withIdentifier: self.identifer) ?? UITableViewCell(style: .default, reuseIdentifier: self.identifer)

        cell.textLabel?.text = self.rootController.restaurants[indexPath.row].name
        return cell
    }

    func onRestaurantReloaded() {
        if (rootController != nil) {
            self.tableViewRestaurant.reloadData()
        }
    }
    
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            if let id = self.rootController.restaurants[indexPath.row].id_host {
                
                SyncEngine.removeRestaurant(restaurant: self.rootController.restaurants[indexPath.row], end: {
                    print("Restaurant id = " + id + " removed")
                    self.rootController.restaurants.remove(at: indexPath.row)
                    self.tableViewRestaurant.deleteRows(at: [indexPath], with: .fade)
                })
                
            } else {
                SyncEngine.removeRestaurant(restaurant: self.rootController.restaurants[indexPath.row], end: {
                    self.rootController.restaurants.remove(at: indexPath.row)
                    self.tableViewRestaurant.deleteRows(at: [indexPath], with: .fade)
                })
            }
            
        } else {
            print("Not handle")
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let addView : AddViewController = AddViewController()
        addView.editRestaurant(rest: self.rootController.restaurants[indexPath.row])
        self.navigationController?.pushViewController(addView, animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if(self.rootController != nil) {
            self.rootController.refresh()
        }
    }
}

