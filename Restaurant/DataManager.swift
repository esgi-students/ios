//
//  DataManager.swift
//  MyFirstCoreData
//
//  Created by Mohsan on 16/11/2016.
//  Copyright © 2016 Mohsan. All rights reserved.
//

import Foundation
import CoreData


class DataManager: NSObject {
    
    public static let shared = DataManager()
    public var objectContext: NSManagedObjectContext?
    
    private override init() {
        
        //Bundle permet de récupérr les fichier dans l'arborescence du projet
        
        if let modelURL = Bundle.main.url(forResource: "Restaurant", withExtension: "momd") {
            
            // Permet de charger le model
            if let model = NSManagedObjectModel(contentsOf: modelURL) {
                
                //Peret de créér une connexion
                let persistentStoreCoordinator = NSPersistentStoreCoordinator(managedObjectModel: model)
                if let dbURL = FileManager.documentURL(childPath: "mydb.db") {
                    
                    print(dbURL)
                    
                    _ = try? persistentStoreCoordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: dbURL , options: nil)
                    let context = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
                    context.persistentStoreCoordinator = persistentStoreCoordinator
                    
                    self.objectContext = context
                    
                }
                
            }
        }
        
        
    }
    
    
}
