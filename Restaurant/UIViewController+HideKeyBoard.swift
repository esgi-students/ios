//
//  UIViewController+HideKeyBoard.swift
//  Restaurant
//
//  Created by Mohsan on 27/11/2016.
//  Copyright © 2016 Mohsan. All rights reserved.
//

import UIKit

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    
    func alertMessage(title: String, message:String, handler : ((UIAlertAction) -> Void)?){
        
        let alertController : UIAlertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let alertAction : UIAlertAction = UIAlertAction(title: "OK", style: .default, handler: handler)
        
        
        alertController.addAction(alertAction)
        
        self.present(alertController, animated: true, completion: nil)
        
        
        
    }
    
}
