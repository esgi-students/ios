//
//  TabBarViewController.swift
//  Restaurant
//
//  Created by Quentin on 03/12/2016.
//  Copyright © 2016 Mohsan. All rights reserved.
//

import UIKit

protocol TabBarViewController {
    func onRestaurantReloaded()
}
