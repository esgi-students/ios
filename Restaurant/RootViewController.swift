
import UIKit
import Alamofire
import SwiftyJSON
import CoreData

class RootViewController: UITabBarController {
    
    var restaurants : [Restaurant] = []
    
    let listRestaurantViewController = ListRestaurantViewController();
    let mapViewController = MapViewController()
    
    var loading: Bool = false;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let listRestaurantVCNav = UINavigationController(rootViewController: listRestaurantViewController)
        let mapVCNav = UINavigationController(rootViewController: mapViewController)
        
        // Tab Bar
        setViewControllers([listRestaurantVCNav, mapVCNav], animated: true)
        
        // Tar Bar Items
        listRestaurantVCNav.tabBarItem = UITabBarItem(title: "Liste Restaurant", image: nil, tag: 1)
        mapVCNav.tabBarItem = UITabBarItem(title: "Maps", image: nil, tag: 2)
        
        loading = true
        SyncEngine.fullSync(res: { (restaurants_) in
            self.invalidateRestaurants(restaurants: restaurants_)
            self.loading = false
        })
        
        self.tabBar.tintColor = UIColor.gray
        self.tabBar.barTintColor = UIColor.white
    }
    
    func refresh() {
        print("Refresh")
        loading = true;
        if Reachability.isConnectedToNetwork() {
            SyncEngine.fullSync(res: { (restaurants_) in
                self.invalidateRestaurants(restaurants: restaurants_)
                self.loading = false
            })
        } else {
            SyncEngine.loadVisible(res: { (restaurants_) in
                self.invalidateRestaurants(restaurants: restaurants_)
                loading = false
                
            })

        }
        
    }
    
    func invalidateRestaurants(restaurants: [Restaurant]) {
        self.restaurants = restaurants
        listRestaurantViewController.onRestaurantReloaded()
        mapViewController.onRestaurantReloaded()
    }
}
